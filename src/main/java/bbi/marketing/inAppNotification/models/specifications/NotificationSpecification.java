package bbi.marketing.inAppNotification.models.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import bbi.marketing.inAppNotification.models.Notification;

public class NotificationSpecification implements Specification<Notification> {

	private String content;
	private String username;
	private String state;
	private String fromDate;
	private String toDate;

	/**
	 * @param content
	 * @param username
	 * @param state
	 * @param fromDate
	 * @param toDate
	 */
	public NotificationSpecification(String content, String username, String state, String fromDate, String toDate) {
		super();
		this.content = content;
		this.username = username;
		this.state = state;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	@Override
	public Predicate toPredicate(Root<Notification> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

		Predicate p = criteriaBuilder.conjunction();

		if (content != null) {
			p.getExpressions().add(criteriaBuilder.and(criteriaBuilder.equal(root.get("message"), content)));
		}
		if (username != null) {
			p.getExpressions().add(criteriaBuilder.and(criteriaBuilder.equal(root.get("receiver"), username)));
		}
		if (state != null) {
			p.getExpressions().add(criteriaBuilder.and(criteriaBuilder.equal(root.get("sendingState"), state)));
		}

		if (fromDate != null && toDate != null) {
			p.getExpressions()
					.add(criteriaBuilder.and(criteriaBuilder.between(root.get("createDateTime"), fromDate, toDate)));
		} else if (fromDate != null) {
			p.getExpressions().add(
					criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.get("createDateTime"), fromDate)));
		} else if (toDate != null) {
			p.getExpressions()
					.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("createDateTime"), toDate)));
		}

		return p;
	}

}
