package bbi.marketing.inAppNotification.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import bbi.marketing.inAppNotification.Repositories.NotificationRepo;
import bbi.marketing.inAppNotification.models.Notification;
import bbi.marketing.inAppNotification.models.NotificationStructure;
import bbi.marketing.inAppNotification.models.specifications.NotificationSpecification;

@Service
public class NotificationService {

	@Autowired
	private NotificationRepo notificationRepo;

	private SimpMessagingTemplate messagingTemplate;

	public void sendAndSaveNotification(NotificationStructure notificationStructure) {
		
		for (String receiver : notificationStructure.getReceivers()) {
			Notification notification = new Notification();
			notification.setMessage(notificationStructure.getMessage());
			notification.setReceiver(receiver);
			notification.setSender(notificationStructure.getFrom());
			notification.setSendingState("pending"); // default state

			// save notification in notification log
			Notification savedNotification = notificationRepo.save(notification);

			// send notification to user
			if (savedNotification != null)
				messagingTemplate.convertAndSendToUser(receiver, "/queue/notify", notification);
			else
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "notification with body: "
						+ notification.getMessage() + " for user " + notification.getReceiver() + " not saved");
		}

	}

	public void updateNotificationState(Notification notification) {
		Optional<Notification> optional = notificationRepo.findById(notification.getId());
		if (optional.isPresent()) {
			Notification previousSavedNotification = optional.get();
			previousSavedNotification.setSendingState("received");
			// update state and save notification
			notificationRepo.save(previousSavedNotification);
		} else
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not found notification. to update its state");

	}

	public Page<Notification> getByContentAndState(String content, String username, String state, Integer page,
			Integer pageSize, String sortingAttrib) {
		// System.out.println("default case");
		NotificationSpecification notificationSpecification = new NotificationSpecification(content, username, state,
				null, null);
		return notificationRepo.findAll(notificationSpecification,
				PageRequest.of(page, pageSize, Sort.Direction.ASC, sortingAttrib));
	}

	public Page<Notification> getByContentAndStateFromDateAndTime(String content, String username, String state,
			Integer page, Integer pageSize, String sortingAttrib, String from) {
		// System.out.println("case 1");
		NotificationSpecification notificationSpecification = new NotificationSpecification(content, username, state,
				from, null);
		return notificationRepo.findAll(notificationSpecification,
				PageRequest.of(page, pageSize, Sort.Direction.ASC, sortingAttrib));
	}

	public Page<Notification> getByContentAndStateToDateAndTime(String content, String username, String state,
			Integer page, Integer pageSize, String sortingAttrib, String toDate) {
		// System.out.println("case 2");
		NotificationSpecification notificationSpecification = new NotificationSpecification(content, username, state,
				null, toDate);

		return notificationRepo.findAll(notificationSpecification,
				PageRequest.of(page, pageSize, Sort.Direction.ASC, sortingAttrib));
	}

	public Page<Notification> getByContentAndStateFromAndToDateAndTime(String content, String username, String state,
			Integer page, Integer pageSize, String sortingAttrib, String fromDate, String toDate) {
		// System.out.println("case 3");
		NotificationSpecification notificationSpecification = new NotificationSpecification(content, username, state,
				fromDate, toDate);
		return notificationRepo.findAll(notificationSpecification,
				PageRequest.of(page, pageSize, Sort.Direction.ASC, sortingAttrib));
	}
}
