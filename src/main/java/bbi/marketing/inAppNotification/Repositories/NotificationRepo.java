package bbi.marketing.inAppNotification.Repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import bbi.marketing.inAppNotification.models.Notification;

public interface NotificationRepo extends CrudRepository<Notification ,Integer> , JpaSpecificationExecutor <Notification> {

    Notification findById(int id);
    List<Notification> findAllByMessageAndSendingState(String message , String state);

}

