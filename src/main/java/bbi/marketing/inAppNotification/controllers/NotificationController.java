package bbi.marketing.inAppNotification.controllers;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;

import com.bbi.marketing.comment.Greeting;
import com.bbi.marketing.comment.HelloMessage;

import bbi.marketing.inAppNotification.models.Notification;
import bbi.marketing.inAppNotification.models.NotificationStructure;
import bbi.marketing.inAppNotification.services.NotificationService;
import bbi.marketing.inAppNotification.validation.NotificationValidation;

@RestController
public class NotificationController {

	@Autowired
	private NotificationService notificationService;

	@MessageMapping("/hello")
	@SendTo("/queue/notify")
	public Greeting greeting(HelloMessage message) throws Exception {
		Thread.sleep(1000); // simulated delay
		return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
	}

	@RequestMapping(value = "/sendNotification", method = RequestMethod.POST)
	public void sendNotification(@RequestBody NotificationStructure notificationStructure) {

		if (NotificationValidation.validateNotificationStructure(notificationStructure))
			notificationService.sendAndSaveNotification(notificationStructure);
	}

	@MessageMapping("/ack")
	public void receiveAcknowledge(@Payload Notification notification) {
		// acknowledge would be Received if notification had sent successfully to user
		notificationService.updateNotificationState(notification);
	}

	@GetMapping(value = "/getNotifications/{content}/{state}")
	public Page<Notification> getNotifications(@PathVariable() String content, @PathVariable() String state,
			@RequestParam @NotNull Integer page, @RequestParam @NotNull Integer pageSize,
			@RequestParam @NotNull String sortingAttrib, @RequestParam(defaultValue = "", required = false) String from,
			@RequestParam(defaultValue = "", required = false) String to, Principal principal) {

		// user name
		String username = principal.getName();

		if (!from.isEmpty() && to.isEmpty())
			return notificationService.getByContentAndStateFromDateAndTime(content, username, state, page, pageSize,
					sortingAttrib, from);
		else if (from.isEmpty() && !to.isEmpty())
			return notificationService.getByContentAndStateToDateAndTime(content, username, state, page, pageSize,
					sortingAttrib, to);
		else if (!from.isEmpty() && !to.isEmpty())
			return notificationService.getByContentAndStateFromAndToDateAndTime(content, username, state, page,
					pageSize, sortingAttrib, from, to);

		return notificationService.getByContentAndState(content, username, state, page, pageSize, sortingAttrib);

	}

	@GetMapping(path = "/logout")
	public void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.logout();
		response.sendRedirect("/");
	}

	// for testing
	@GetMapping(path = "/customers")
	public String customers(Principal principal) {

		return "Hello " + principal.getName() + "<br><br> " + "<a href='/index2.html'>Chat room </a> " + "<br><br> "
				+ "<a href='/logout'>Logout</a>";

	}

}
