package bbi.marketing.inAppNotification.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import bbi.marketing.inAppNotification.models.NotificationStructure;

public class NotificationValidation {

	public static Boolean validateNotificationStructure(NotificationStructure notificationStructure) {

		if (notificationStructure.getMessage().trim().isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " message must not be empty ");
		if (notificationStructure.getFrom().trim().isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " from must not be empty ");
		if (notificationStructure.getReceivers().isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " receiver list must not be empty ");
		for (String receiver : notificationStructure.getReceivers()) {
			if (receiver.trim().isEmpty())
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " receiver username must not be empty ");

		}
		return true;

	}

}
